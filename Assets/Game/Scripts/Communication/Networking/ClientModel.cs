namespace MassZames.Communication.Networking
{
    public class ClientModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}