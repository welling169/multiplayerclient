using System;
using System.Collections.Generic;
using System.Linq;
using MassZames.Player;
using VContainer.Unity;

namespace MassZames.World
{
    public class WorldController : IInitializable, IDisposable
    {
        private readonly Func<IPlayerModel, IPlayerController> _playerFactory;
        private readonly IWorldUpdater _worldUpdater;

        private readonly Dictionary<string, (IPlayerController, IPlayerModel)> _playerControllers = new Dictionary<string, (IPlayerController, IPlayerModel)>();

        public WorldController(
            IWorldUpdater worldUpdater,
            Func<IPlayerModel, IPlayerController> playerFactory)
        {
            _worldUpdater = worldUpdater;
            _playerFactory = playerFactory;
        }

        public void Initialize()
        {
            _worldUpdater.WorldUpdated += OnWorldUpdated;
        }

        public void Dispose()
        {
            _worldUpdater.WorldUpdated -= OnWorldUpdated;
        }

        private void OnWorldUpdated()
        {
            UpdatePlayers();
        }

        private void UpdatePlayers()
        {
            var newPlayers = _worldUpdater.Players.Keys.Except(_playerControllers.Keys).ToList();
            var disconnectedPlayers = _playerControllers.Keys.Except(_worldUpdater.Players.Keys).ToList();

            foreach (var newPlayer in newPlayers)
            {
                var model = _worldUpdater.Players[newPlayer];
                var newPlayerController = _playerFactory.Invoke(model);
                _playerControllers.Add(newPlayer, (newPlayerController, model));
            }

            foreach (var disconnectedPlayer in disconnectedPlayers)
            {
                if (_playerControllers.TryGetValue(disconnectedPlayer, out var disconnectedPlayerController))
                {
                    disconnectedPlayerController.Item1.Dispose();
                    _playerControllers.Remove(disconnectedPlayer);
                }
            }

            foreach (var valueTuple in _playerControllers)
            {
                valueTuple.Value.Item2.Position = _worldUpdater.Players[valueTuple.Key].Position;
            }
        }
    }
}