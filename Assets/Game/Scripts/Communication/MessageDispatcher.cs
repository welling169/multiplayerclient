using System;
using System.Collections.Generic;
using Welling.Multiplayer.Core;

namespace MassZames.Communication
{
    public class MessageDispatcher : IMessageDispatcher
    {
        public event Action<Message> MessageReceived;

        private readonly Dictionary<Type, Action<Message>> _subscribers = new Dictionary<Type, Action<Message>>();

        public void Subscribe<T>(Action<T> callback) where T : Message
        {
            // _subscribers.Add(typeof(T), Convert.ChangeType(callback, typeof(Action<Message>)) as Action<Message>);
        }

        public void Unsubscribe<T>() where T : Message
        {
            // _subscribers.Remove(typeof(T));
        }

        public void DispatchMessage(Message message)
        {
            var type = message.GetType();
            
            if (_subscribers.TryGetValue(type, out var callback))
            {
                callback?.Invoke(message);
            }

            MessageReceived?.Invoke(message);
        }
    }
}