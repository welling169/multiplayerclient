using System;
using System.Collections.Generic;
using MassZames.Communication;
using MassZames.Player;
using MassZamesContracts.Messages;
using UnityEngine;
using VContainer.Unity;
using Welling.Multiplayer.Core;

namespace MassZames.World
{
    public class WorldUpdater : IWorldUpdater, IStartable, IDisposable
    {
        public event Action WorldUpdated;

        private readonly IMessageDispatcher _messageDispatcher;

        public Dictionary<string, IPlayerModel> Players { get; } = new Dictionary<string, IPlayerModel>();

        public WorldUpdater(IMessageDispatcher messageDispatcher)
        {
            _messageDispatcher = messageDispatcher;
        }

        public void Start()
        {
            _messageDispatcher.MessageReceived += OnMessageReceived;
        }

        public void Dispose()
        {
            _messageDispatcher.MessageReceived -= OnMessageReceived;
        }

        private void OnMessageReceived(Message message)
        {
            if (message is ServerWorldUpdateMessage worldUpdateMessage)
            {
                OnMessageReceived(worldUpdateMessage);
            }
        }

        private void OnMessageReceived(ServerWorldUpdateMessage worldUpdateMessage)
        {
            foreach (var playerData in worldUpdateMessage.PlayerDatas)
            {
                if (Players.ContainsKey(playerData.Id))
                {
                    var model = Players[playerData.Id];
                    model.Position = playerData.Position.GetUnityVector();
                    model.Direction = playerData.Direction.GetUnityVector();
                    if (playerData.IsDash)
                    {
                        model.DashTime = playerData.DashTime;
                        model.IsDash = true;
                    }
                }
                else
                {
                    Players[playerData.Id] = new PlayerModel
                    {
                        Id = playerData.Id, Name = playerData.Name,
                        Position = playerData.Position.GetUnityVector(),
                        DashTime = playerData.DashTime,
                        Direction = playerData.Direction.GetUnityVector()
                    };
                }
            }

            WorldUpdated?.Invoke();
        }
    }
}