using System;
using System.Threading;
using MassZames.Communication.Networking;
using MassZamesContracts.Messages;
using UnityEngine;
using VContainer.Unity;

namespace MassZames
{
    public class InputController : ITickable
    {
        private readonly Client _client;
        private readonly ClientModel _clientModel;
        private readonly ClientSettings _clientSettings;

        private readonly InputData _inputData = new InputData();

        public InputController(Client client, ClientModel clientModel, ClientSettings clientSettings)
        {
            _client = client;
            _clientModel = clientModel;
            _clientSettings = clientSettings;
        }

        public async void Tick()
        {
            var previousInputData = _inputData.Clone();
            if (_clientModel.Id == null)
            {
                return;
            }

            try
            {
                var input = new Vector2();
                if (Input.GetKey(KeyCode.A))
                {
                    input.x = -1f;
                }

                if (Input.GetKey(KeyCode.D))
                {
                    input.x = 1f;
                }

                if (Input.GetKey(KeyCode.W))
                {
                    input.y = 1f;
                }

                if (Input.GetKey(KeyCode.S))
                {
                    input.y = -1f;
                }

                if (Input.GetKeyDown(KeyCode.Space))
                {
                    _inputData.IsDash = true;
                }

                _inputData.Direction = input;

                if (!previousInputData.Equals(_inputData))
                {
                    var message = new ClientInputMessage()
                    {
                        MoveDirection = new Vector2Serializable() {x = input.x, y = input.y},
                        IsDash = _inputData.IsDash
                    };

                    await _client.SendTCPMessage(message, CancellationToken.None);
                }

                _inputData.IsDash = false;

                if (Input.GetKeyDown(KeyCode.K))
                {
                    var updateSettingsMessage = new UpdateSettingsClientMessage()
                    {
                        Speed = _clientSettings.Speed,
                        AccelerationSpeed = _clientSettings.AccelerationSpeed,
                        DecelerationSpeed = _clientSettings.DecelerationSpeed,
                        MaxSpeed = _clientSettings.MAXSpeed,
                        TresholdForStop = _clientSettings.TresholdForStop,
                        DashMultiplier = _clientSettings.DashSpeedMultiplier,
                        DashTime = _clientSettings.DashTime
                    };
                    await _client.SendTCPMessage(updateSettingsMessage, CancellationToken.None);
                }
            }
            catch (Exception exception)
            {
                Debug.LogError($"Failed to send update {exception}");
            }
        }
    }
}