using System;

namespace MassZames.Player
{
    public interface IPlayerController : IDisposable
    {
        void SetupModel(IPlayerModel playerModel);
    }
}