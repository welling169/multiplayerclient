using System;
using UnityEngine;

namespace MassZames
{
    public class InputData : IEquatable<InputData>
    {
        public Vector2 Direction { get; set; }
        public bool IsDash { get; set; }

        public InputData Clone()
        {
            var clone = new InputData();
            clone.Direction = Direction;

            return clone;
        }

        public bool Equals(InputData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Direction.Equals(other.Direction) && IsDash == other.IsDash;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((InputData) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Direction.GetHashCode() * 397) ^ IsDash.GetHashCode();
            }
        }
    }
}