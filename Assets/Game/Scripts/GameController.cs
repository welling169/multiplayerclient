using System;
using System.Threading;
using MassZames.Communication.Networking;
using UnityEngine;
using VContainer.Unity;

namespace MassZames
{
    public class GameController : IStartable
    {
        private readonly Client _client;
        private readonly Configuration _configuration;

        public GameController(Client client, Configuration configuration
        )
        {
            _client = client;
            _configuration = configuration;
        }

        public async void Start()
        {
            try
            {
                await _client.ConnectAsync(_configuration.Ip, _configuration.Port, CancellationToken.None);
            }
            catch (Exception exception)
            {
                Debug.LogError($"Exception {exception}");
            }
        }
    }
}