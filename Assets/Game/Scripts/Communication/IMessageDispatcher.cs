using System;
using Welling.Multiplayer.Core;

namespace MassZames.Communication
{
    public interface IMessageDispatcher
    {
        event Action<Message> MessageReceived;
        void Subscribe<T>(Action<T> callback) where T : Message;
        void Unsubscribe<T>() where T : Message;
        void DispatchMessage(Message message);
    }
}