using UnityEngine;

namespace MassZames.Player
{
    public interface IPlayerModel
    {
        public string Id { get; }
        public string Name { get; }
        public Vector2 Position { get; set; }
        public Vector2 Direction { get; set; }
        public bool IsDash { get; set; }
        public float DashTime { get; set; }
    }
}