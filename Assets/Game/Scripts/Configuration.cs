using UnityEngine;

namespace MassZames
{
    [CreateAssetMenu]
    public class Configuration : ScriptableObject
    {
        [SerializeField] private bool _isRemoteServer = default;
        [SerializeField] private string _localIp = default;
        [SerializeField] private string _remoteIp = default;
        [SerializeField] private int _port = default;

        public string Ip => _isRemoteServer ? _remoteIp : _localIp;
        public int Port => _port;
    }
}