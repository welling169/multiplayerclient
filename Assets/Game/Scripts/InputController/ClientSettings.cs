using UnityEngine;

namespace MassZames
{
    [CreateAssetMenu]
    public class ClientSettings : ScriptableObject
    {
        [SerializeField] private float _speed = 5f;
        [SerializeField] private float _maxSpeed = 10f;
        [SerializeField] private float _accelerationSpeed = 10f;
        [SerializeField] private float _decelerationSpeed = 3f;
        [SerializeField] private float _tresholdForStop = 0.5f;

        [SerializeField] private float _dashSpeedMultiplier = 5f;
        [SerializeField] private float _dashTime = 0.5f;
        
        public float Speed => _speed;

        public float MAXSpeed => _maxSpeed;

        public float AccelerationSpeed => _accelerationSpeed;

        public float DecelerationSpeed => _decelerationSpeed;

        public float TresholdForStop => _tresholdForStop;

        public float DashSpeedMultiplier => _dashSpeedMultiplier;

        public float DashTime => _dashTime;
    }
}