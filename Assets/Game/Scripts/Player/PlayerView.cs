using UnityEngine;

namespace MassZames.Player
{
    public class PlayerView : MonoBehaviour
    {
        [SerializeField] private float _dashSqueeze = 2f;

        public float DashSqueeze => _dashSqueeze;
    }
}