using UnityEngine;

namespace MassZames.Player
{
    public class PlayerModel : IPlayerModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public Vector2 Position { get; set; }
        public Vector2 Direction { get; set; }
        public bool IsDash { get; set; }
        public float DashTime { get; set; }
    }
}