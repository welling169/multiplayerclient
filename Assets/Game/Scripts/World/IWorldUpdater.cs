using System;
using System.Collections.Generic;
using MassZames.Player;

namespace MassZames.World
{
    public interface IWorldUpdater
    {
        event Action WorldUpdated;

        Dictionary<string, IPlayerModel> Players { get; }
    }
}