using DG.Tweening;
using MassZames.World;
using UnityEngine;

namespace MassZames.Player
{
    public class PlayerController : IPlayerController
    {
        private readonly PlayerView _playerView;
        private readonly IWorldUpdater _worldUpdater;
        private IPlayerModel _playerModel;

        public PlayerController(
            PlayerView playerView,
            IWorldUpdater worldUpdater)
        {
            _playerView = playerView;
            _worldUpdater = worldUpdater;
            _worldUpdater.WorldUpdated += WorldUpdaterOnWorldUpdated;
        }

        private void WorldUpdaterOnWorldUpdated()
        {
            _playerView.transform.position = _playerModel.Position;
            var dir = _playerModel.Direction;
            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + 90f;
            _playerView.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            if (_playerModel.IsDash)
            {
                _playerModel.IsDash = false;
                var sequence = DOTween.Sequence();
                sequence.Append(_playerView.transform.DOScaleX(_playerView.DashSqueeze, _playerModel.DashTime / 2f)
                    .SetEase(Ease.OutQuart));
                sequence.Append(_playerView.transform.DOScaleX(1f, _playerModel.DashTime / 2f).SetEase(Ease.InQuart));
                sequence.Play();
            }
        }

        public void Dispose()
        {
            _worldUpdater.WorldUpdated -= WorldUpdaterOnWorldUpdated;
            if (_playerView != null)
            {
                Object.Destroy(_playerView.gameObject);
            }
        }

        public void SetupModel(IPlayerModel playerModel)
        {
            _playerModel = playerModel;
        }
    }
}