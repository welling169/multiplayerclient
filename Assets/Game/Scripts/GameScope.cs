using MassZames.Communication;
using MassZames.Communication.Networking;
using MassZames.Player;
using MassZames.World;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace MassZames
{
    public class GameScope : LifetimeScope
    {
        [SerializeField] private PlayerView _playerViewPrefab;
        [SerializeField] private Transform _playersParent;
        [SerializeField] private Configuration _configuration;
        [SerializeField] private ClientSettings _clientSettings;

        protected override void Configure(IContainerBuilder builder)
        {
            builder.Register<MessageDispatcher>(Lifetime.Singleton).AsImplementedInterfaces();
            builder.Register<WorldUpdater>(Lifetime.Singleton).AsImplementedInterfaces();
            builder.Register<Client>(Lifetime.Singleton).AsImplementedInterfaces().AsSelf();
            builder.Register<ClientModel>(Lifetime.Singleton);
            builder.Register<InputController>(Lifetime.Singleton).AsImplementedInterfaces();
            builder.RegisterInstance(_configuration);
            builder.RegisterInstance(_clientSettings);

            builder.RegisterEntryPoint<GameController>();
            builder.RegisterEntryPoint<WorldController>();
            builder.Register<PlayerController>(Lifetime.Transient).AsImplementedInterfaces();
            builder.RegisterComponentInNewPrefab(_playerViewPrefab, Lifetime.Transient).UnderTransform(_playersParent);
            builder.RegisterFactory<IPlayerModel, IPlayerController>(
                container =>
                {
                    return playerModel =>
                    {
                        var playerController = container.Resolve<IPlayerController>();
                        playerController.SetupModel(playerModel);
                        return playerController;
                    };
                }, Lifetime.Scoped);
        }
    }
}