using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using MassZames.Utils;
using MassZamesContracts.Messages;
using MultiplayerTools.Multiplayer;
using UnityEngine;
using Welling.Multiplayer.Core;

namespace MassZames.Communication.Networking
{
    public class Client : IDisposable
    {
        private readonly IMessageDispatcher _messageDispatcher;
        private readonly TcpClient _tcpClient;
        private readonly CancellationTokenSource _cancellationTokenSource;
        private readonly ClientModel _clientModel;
        private PackageHandler _packageHandler;
        private UdpClient _udpClient;
        private IPEndPoint _ipEndPoint;

        public Client(IMessageDispatcher messageDispatcher, ClientModel clientModel)
        {
            _messageDispatcher = messageDispatcher;
            _clientModel = clientModel;
            _tcpClient = new TcpClient();
            _cancellationTokenSource = new CancellationTokenSource();
        }

        public void Dispose()
        {
            _udpClient.Close();
            _tcpClient.Close();
            _tcpClient.Dispose();
            _cancellationTokenSource.Cancel();
            _cancellationTokenSource.Dispose();
        }

        private async void ListenTCPMessagesAsync()
        {
            try
            {
                while (!_cancellationTokenSource.Token.IsCancellationRequested && _tcpClient.Connected)
                {
                    var message = await _packageHandler.ReadMessageAsync(_cancellationTokenSource.Token);

                    if (message == null)
                    {
                        Debug.Log("Disconnected");
                        return;
                    }

                    if (message is ServerHelloMessage serverHelloMessage)
                    {
                        _clientModel.Id = serverHelloMessage.Id;
                        Debug.Log($"Hello message received. Id {_clientModel.Id}");
                        await SendTCPMessage(new ClientHelloMessage(), _cancellationTokenSource.Token);
                        SendUDPMessage(new ClientHelloMessage());
                        continue;
                    }

                    _messageDispatcher.DispatchMessage(message);
                }
            }
            catch (ObjectDisposedException)
            {
                //disconnected
            }
            catch (Exception e)
            {
                Debug.LogError($"Messages listening ended {e}");
            }
        }

        private async void ListenUDPMessagesAsync()
        {
            try
            {
                while (!_cancellationTokenSource.Token.IsCancellationRequested)
                {
                    var udp = _udpClient.Receive(ref _ipEndPoint);

                    if (udp.Length < 4)
                    {
                        return;
                    }

                    var message = PackageHandler.GetMessageFromData(udp);

                    ThreadManager.ExecuteOnMainThread(() => { _messageDispatcher.DispatchMessage(message); });
                }
            }
            catch (SocketException)
            {
            }
            catch (ObjectDisposedException)
            {
                //disconnected
            }
            catch (Exception e)
            {
                Debug.LogError($"Messages listening ended {e}");
            }
        }

        public async Task ConnectAsync(string ip, int port, CancellationToken cancellationToken)
        {
            await _tcpClient.ConnectAsync(ip, port, cancellationToken);
            var localPort = ((IPEndPoint) _tcpClient.Client.LocalEndPoint).Port;
            _udpClient = new UdpClient(localPort);
            _ipEndPoint = new IPEndPoint(IPAddress.Parse(ip), port);
            _udpClient.Connect(_ipEndPoint);
            _packageHandler = new PackageHandler(_tcpClient.GetStream());
            ListenTCPMessagesAsync();
            new Thread(ListenUDPMessagesAsync).Start();
        }

        public async Task SendTCPMessage(ClientMessage message, CancellationToken cancellationToken)
        {
            message.Id = _clientModel.Id;
            await _packageHandler.SendMessageAsync(message, cancellationToken);
        }

        public void SendUDPMessage(ClientMessage message)
        {
            message.Id = _clientModel.Id;
            var data = PackageHandler.GetPacket(message);
            _udpClient.Send(data.ToArray(), data.Length);
        }
    }
}